PImage img;
int pixelSize = 5;

int[] grade = new int[256];
int countX = 0;
int countY = 0;

void setup() {
  size(800, 800);
  img = loadImage("Image-3.jpg");

  for (int i=0; i<256; i++) {
    grade[i] = 0;
  }
  background(0);

  //image(img, 0, 0);
  //worldRecord = 0;
}

void draw() {
  background(0);
  pixelSize = int(map(mouseX, 0, width, 1, 30));
  for (int i=0; i<256; i++) {
    grade[i] = 0;
  }
  for (int y=0; y<height/2; y=y+pixelSize) {
    for (int x=0; x<width; x=x+pixelSize) {
      int index = x + y*width;
      color c = img.pixels[index];
      float r = red(c);
      float g = green(c);
      float b = blue(c);
      float br = brightness(c);
      grade[int(br)] = grade[int(br)] + 1;      
      fill(c);
      noStroke();
      rect(x, y, pixelSize, pixelSize);
    }
  }
  //println(grade);
  countY = 0;
  for (int i=0; i<256; i++) {
    fill(i);
    noStroke();
    for (int k=0; k<grade[i]; k++) { 
      rect(countX*pixelSize, height/2+countY*pixelSize, pixelSize, pixelSize);
      countX++;
      if(countX*pixelSize > width){
        countX = 0;
        countY++;
      }
    }
  }
}

