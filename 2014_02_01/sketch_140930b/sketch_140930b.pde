void setup(){
  size(600, 600);
  background(0);
}

void draw(){
  background(0);
  PVector u = new PVector(mouseX, mouseY);
  PVector v = new PVector(width/2, height/2);
  u.sub(v);
  //u.mult(0.5);
  //u.normalize();
  //u.mult(50);
  
  float m = u.mag();
  
  pushMatrix();
  translate(width/2, height/2);
  stroke(255);
  line(0, 0, u.x, u.y);
  popMatrix();
  
  fill(255);
  noStroke();
  rect(0,0,m,20);
}
