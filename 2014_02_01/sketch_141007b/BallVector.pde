class BallVector {
  PVector pos;
  PVector speed;
  PVector acc;

  BallVector() {
    pos = new PVector(random(width), random(150));
    speed = new PVector(0, 0);
    acc = new PVector(0.0, 0.0);
  }
  
  void addForce(PVector f){
    acc.add(f);
  }

  void update() {
    speed.add(acc);
    pos.add(speed); 
    acc.mult(0);

    if (pos.x > width) {
      pos.x = 0;
      //speed.x *= -1;
    }
    if (pos.x < 0) {
      pos.x = width;
      //speed.x *= -1;
    }
    if (pos.y > height) {
      pos.y = height;
      speed.y *= -1;
    }
    if (pos.y < 0) {
      pos.y = 0;
      speed.y *= -1;
    }
  }

  void display() {
    fill(255);
    noStroke();
    ellipse(pos.x, pos.y, 30, 30);
  }
}

