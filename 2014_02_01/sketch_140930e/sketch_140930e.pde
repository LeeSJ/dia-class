BallVector[] b;

void setup() {
  size(500, 500);
  background(0);

  b = new BallVector[30];
  for (int i=0; i<b.length; i++) {
    b[i] = new BallVector();
  }
}

void draw() {
  background(0);

  for (int i=0; i<b.length; i++) {
    PVector g = new PVector(0.0, 0.4);
    b[i].addForce(g);
    if (keyPressed) {
      PVector w = new PVector(0.2, 0.0);
      b[i].addForce(w);
    }
    b[i].update();
    b[i].display();
  }
}

