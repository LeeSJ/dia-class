PVector[] m;
float x;
float y;
float t = 0.0;
float dia = 200;
float speedDia = 5;

int lineNum = 50;

PImage img;

void setup() {
  img = loadImage("face_w.jpg");
  size(img.width, img.height);
  m = new PVector[lineNum*lineNum];
  for (int i=0; i<lineNum; i++) {
    for (int j=0; j<lineNum; j++) {
      int index = j+i*lineNum;
      m[index] = new PVector(j*(width/lineNum)+(width/lineNum)/2, i*(width/lineNum)+(width/lineNum)/2);
    }
  }
}

void draw() {
  background(0);
  x = width/2+cos(t)*dia;
  y = height/2+sin(t)*dia;
  for (int i=0; i<lineNum; i++) {
    for (int j=0; j<lineNum; j++) {
      int index = j+i*lineNum;
      PVector w = new PVector(x, y);
      w.sub(m[index]);
      w.setMag(width/lineNum+map(mouseX, 0, width, -width/lineNum, 50));
      //stroke(255);
      //strokeWeight(1);
      //line(j*(width/lineNum)+(width/lineNum)/2, i*(width/lineNum)+(width/lineNum)/2, j*(width/lineNum)+(width/lineNum)/2+w.x, i*(width/lineNum)+(width/lineNum)/2+w.y);
      //line(m[index].x, m[index].y, j*(width/lineNum)+(width/lineNum)/2+w.x,
      //i*(width/lineNum)+(width/lineNum)/2+w.y);
      noStroke();
      int pixelIndex = int(m[index].x + m[index].y*width);
      color c = img.pixels[pixelIndex];
      fill(c);
      ellipse(j*(width/lineNum)+(width/lineNum)/2+w.x,i*(width/lineNum)+(width/lineNum)/2+w.y, 8, 8);
    }
  }
  //fill(255);
  noFill();
  //ellipse(x, y, 20, 20);
  t+=0.01;
  dia = dia+speedDia;
  if (dia >= width/2) {
    dia = width/2;
    speedDia *= -1;
  }
  if (dia <= 1) {
    dia = 1;
    speedDia *= -1;
  }
}

