class Ball {
  float x, y;
  float speedX, speedY;
  float accX, accY;
  int dia;
  int type;
  int dirX, dirY;
  float maxSpeed;
  
  int lifeTime;

  Ball(int _i) {
    x = width/2;
    y = height/2;
    accX = random(-0.05, 0.05);
    accY = random(-0.05, 0.05);
    speedX = 0;
    speedY = 0;
    dia = 20;
    type = _i;
    dirX = 1;
    dirY = 1;
    maxSpeed = random(6, 9);
    
    lifeTime = 0;
  }

  void update() {
    speedX = speedX + accX;
    speedY = speedY + accY;
    x = x + speedX*dirX;
    y = y + speedY*dirY;

    if (speedX > maxSpeed) {
      speedX = maxSpeed;
    }else if(speedX < -maxSpeed){
      speedX = -maxSpeed;
    }
    if (speedY > maxSpeed) {
      speedY = maxSpeed;
    }else if(speedY < -maxSpeed){
      speedY = -maxSpeed;
    }

    if (x > width-dia/2) {
      x = width-dia/2;
      dirX = dirX*-1;
    }
    if (x < dia/2) {
      x = dia/2;
      dirX = dirX*-1;
    }
    if (y > height-dia/2) {
      y = height-dia/2;
      dirY = dirY*-1;
    }
    if (y < dia/2) {
      y = dia/2;
      dirY = dirY*-1;
    }
    
    lifeTime++;
  }

  void display() {
    if (type%2 == 0) {
      fill(255);
      noStroke();
      ellipse(x, y, dia, dia);
    } else {
      stroke(255);
      noFill();
      ellipse(x, y, dia-7, dia-7);
    }
  }
  
  boolean isDead(){
    if(lifeTime > 500){
      return true;
    }else{
      return false;
    }
  }
}
