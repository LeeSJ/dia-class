IntList randomDrunk;
int count = 0;

void setup(){
  size(100, 100);
  background(0);
  randomDrunk = new IntList(); 
  newRandomDrunk(5);
}

void draw(){
  
}

void newRandomDrunk(int _range){
  if(randomDrunk != null){
    randomDrunk.clear();
  }
  for(int i=0; i<_range; i++){
    randomDrunk.append(i);
  }
  randomDrunk.shuffle();
  println(randomDrunk);
}

void mousePressed(){
  println(randomDrunk.get(count));
  count++;
  if(count > randomDrunk.size()-1){
    count = 0;
    //newRandomDrunk(int(random(3, 10)));
    newRandomDrunk(5);
  }
}
