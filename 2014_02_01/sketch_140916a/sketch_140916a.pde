Ball[] b;
int ballNum = 200;
boolean t = true;
boolean tb = true;

void setup() {
  size(600, 600);
  background(0);
  b = new Ball[ballNum];
  for (int i=0; i<ballNum; i++) {
    b[i] = new Ball(i);
  }
}

void draw() {
  //background(0);
  noStroke();
  fill(0, 10);
  rect(0, 0, width, height);
  for (int i=0; i<ballNum; i++) {
    b[i].accX = random(-0.5, 0.5);
    b[i].accY = random(-0.5, 0.5);
    b[i].update();
    b[i].display();
  }
  /*if (second()%3 == 0 && tb == true) {
   t = !t;
   if (t == true) {
   for (int i=0; i<ballNum; i++) {
   b[i].accX = random(-0.05, 0.05);
   b[i].accY = random(-0.05, 0.05);
   }
   } else {
   for (int i=0; i<ballNum; i++) {
   b[i].x = width/2;
   b[i].y = height/2;
   b[i].speedX = 0;
   b[i].speedY = 0;
   b[i].accX = 0;
   b[i].accY = 0;
   }
   }
   tb = false;
   } else if (second()%3 != 0 && tb == false) {
   tb = true;
   }*/
  if (second()%2 == 0 && tb == true) {
    for (int i=0; i<ballNum; i++) {
      if (i%2 == 1) {
        b[i].x = random(50, width-50);
        b[i].y = random(50, height-50);
      } else {
        b[i].x = width/2;
        b[i].y = height/2;
      }
      b[i].speedX = 0;
      b[i].speedY = 0;
      b[i].accX = 0;
      b[i].accY = 0;
    }
    tb = false;
  } else if (second()%2 != 0 && tb == false) {
    tb = true;
  }
}

