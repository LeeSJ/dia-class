PVector pos;
PVector vel;
PVector acc;
float maxVel;

void setup(){
  size(600, 600);
  pos = new PVector(width/2, height/2);
  vel = new PVector(0,0);
  acc = new PVector(0.0, 0.0);
}

void draw(){
  background(0);
  fill(255);
  if(keyPressed){
    PVector w = new PVector(0.2, 0.0);
    acc.add(w);
  }
  PVector g = new PVector(0.0, 0.4);
  acc.add(g);
  
  vel.add(acc);
  pos.add(vel);
  ellipse(pos.x, pos.y, 30, 30);
  
  acc.mult(0);
  
  if(pos.x > width){
    pos.x = width;
    vel.x = vel.x*-1;
  }
  if(pos.x < 0){
    pos.x = 0;
    vel.x = vel.x*-1;
  }
  if(pos.y > height){
    pos.y = height;
    vel.y = vel.y*-1;
  }
  if(pos.y < 0){
    pos.y = 0;
    vel.y = vel.y*-1;
  }
  //println(vel.mag());
}
